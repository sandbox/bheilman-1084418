<?php
// $Id$

/**
 * @file
 * Administration page callbacks for the representative module.
 */

/**
 * Form Builder.  Configure representative.
 */
function representative_admin_settings() {
  // This method of giving prifile fields feels dirty
  // should use _profile_get_fields

  // Get Profile fields
  $result = db_query('SELECT fid, title, name FROM {profile_fields}');

  $options = array();
  while ($field = db_fetch_array($result)) {
    $options[$field['fid']] = $field['title'];
  }

  $form['representative_profile_fields'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Choose profile fields to segment user base'),
    '#options' => $options,
    '#default_value' => variable_get('representative_profile_fields', array()),
    '#description' => t('Some Description'),
  );

  $form = system_settings_form($form);
  $form['#submit'][] = 'representative_admin_settings_submit';

  return $form;
}

function representative_admin_settings_submit($form, &$form_state) {
  $profile_fields = variable_get('representative_profile_fields',array());
  $table_fields = array();

  $sql = "SELECT fid,mapping FROM {representative_fields}";
  $result = db_query($sql);
  while ($row = db_fetch_array($result)) {
    $table_fields[$row['fid']] = $row['mapping'];
  }

  dd($profile_fields, 'Profile Fields');
  dd($table_fields, 'Table Fields');

  $add_fields = array();
  foreach($profile_fields as $key => $value) {
    if ($value === 0) { continue; }
    if (! array_key_exists($key, $table_fields) ) {
      $add_fields[] = $key;
    }
  }
  dd($add_fields, 'Add Fields');

  $remove_fields = array();
  foreach($table_fields as $key => $value) {
    if ( array_key_exists($key, $profile_fields) && ($profile_fields[$key] !== 0) ) {
      // Profile Field already mapped
      continue;
    }
    $remove_fields[] = $key;
  }
  dd($remove_fields, 'Remove Fields');

  
  if ( count($add_fields) !== 0 || count($remove_fields) !== 0 ) {
    dd('yes', 'Tables Dirty');
    db_query('DELETE FROM {representative_usermap}');
    db_query('DELETE FROM {representative_cells}');
    db_query('DELETE FROM {representative_fields}');
  } else {
    dd('no', 'Tables Dirty');
    return;
  }

  // Populate the representative_fields table with fid -> column mappings
  _representative_create_fields($profile_fields);

  // Populate the representative_cells with cross of possible profile_values
  _representative_create_cells();

  // Populate the representative_usermap with uid -> cell mappings
  _representative_create_usermap();

  return;
}